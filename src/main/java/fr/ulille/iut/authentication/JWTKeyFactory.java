package fr.ulille.iut.authentication;

import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.Key;

import java.util.Base64;

/*
 * La génération du token repose sur une clé.
 * La librairie Java JWT propose une méthode simple de génération de clé.
 * Celle-ci doit être réalisée une fois pour l'application. C'est ce qui est fait dans ce Singleton.
 * Explications sur la génération de la clé nécessaire :
 * https://stackoverflow.com/questions/41661821/java-lang-illegalargumentexception-a-signing-key-must-be-specified-if-the-speci
 */
public class JWTKeyFactory {

    private static String base64SecretBytes;
    
    public static String getSecretBytes() {
	if ( base64SecretBytes == null ) {
	    Key secret = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	    byte[] secretBytes = secret.getEncoded();
	    base64SecretBytes = Base64.getEncoder().encodeToString(secretBytes);
	}

	return base64SecretBytes;
    }
}
