package fr.ulille.iut.authentication;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

import javax.ws.rs.Priorities;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Configuration;

import javax.ws.rs.ext.Provider;

import javax.annotation.Priority;

import java.io.IOException;

import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureException;

import fr.ulille.iut.EntryPoint;

/* 
 * Filtre permettant de gérer l'authentification du client. Celui-ci va vérifier le token fournit par le client ou
 * rediriger vers la page d'authentification pour les ressources/méthodes annotées avec @Secured.
 *
 * réalisé d'après :
 * https://stackoverflow.com/questions/26777083/best-practice-for-rest-token-based-authentication-with-jax-rs-and-jersey
 * https://antoniogoncalves.org/2016/10/03/securing-jax-rs-endpoints-with-jwt/
 * et JAX-RS 2.1 Final Release - 6.2 Filters et 6.6 Priorities
 */
@Provider
@Secured
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    private String base64SecretBytes;
    
    public AuthenticationFilter() {
	base64SecretBytes = JWTKeyFactory.getSecretBytes();
    }
    
    @Override
    public void filter(ContainerRequestContext requestContext)
	throws IOException {

	/*
	  Si le client est authentifié, il doit fournir un token dans le champ d'entête HTTP Authorization.
	  Ce champ à la forme Authorization: Bearer { token }
	*/
	String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
	if ( authorizationHeader != null && authorizationHeader.startsWith("Bearer ") ) {
	    String authenticationToken = authorizationHeader.substring(7);

	    Claims claims = null;
	    try {
		claims = Jwts.parser()
		    .setSigningKey(base64SecretBytes)
		    .parseClaimsJws(authenticationToken).getBody();

		
	    } catch ( SignatureException e) {
		e.printStackTrace();
		raiseUnauthorized(requestContext);
	    }
	}
	else {
	    raiseUnauthorized(requestContext);
	}
    }

    private void raiseUnauthorized(ContainerRequestContext requestContext) {
	// Le jeton n'a pas été validé ou n'est pas présent.
	// On renvoie une réponse 401 Unauthorized
	// On fixe le champ d'entête WWW-Authenticate pour indiquer le mode d'authentification
	// (voir https://tools.ietf.org/html/rfc7235#page-7)
	requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
				 .header(HttpHeaders.WWW_AUTHENTICATE, "JWT realm=\"restapi\"")
				 .build());
    }
}
