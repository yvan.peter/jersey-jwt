package fr.ulille.iut;

import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationPath("/api/v1")
public class EntryPoint extends Application {
    final static Logger logger = LoggerFactory.getLogger(EntryPoint.class);
    
    /*
     * Manière standard et portable de déclarer les ressources de l'application REST
     */
    @Override
    public Set<Class<?>> getClasses() {
	Set<Class<?>> s = new HashSet<Class<?>>();
	s.add(fr.ulille.iut.MyResource.class);
	s.add(fr.ulille.iut.api.AuthRessource.class);
	s.add(fr.ulille.iut.authentication.AuthenticationFilter.class);
	return s;
    }
}
