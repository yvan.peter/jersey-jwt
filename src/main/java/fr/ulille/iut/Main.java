package fr.ulille.iut;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

public class Main {
    public static final String BASE_URI = "http://localhost:8080/app/";
	
    static HttpServer startServer() throws IOException {

	URI baseUri = UriBuilder.fromUri("http://localhost/app").port(8080).build();
	
	HttpServer server = GrizzlyHttpServerFactory.createHttpServer(baseUri, new EntryPoint());

	// start the server
        server.start();
	
        return server;
    }

    
}

    
