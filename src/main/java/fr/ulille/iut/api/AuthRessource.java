package fr.ulille.iut.api;

import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.WebApplicationException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.UUID;

import fr.ulille.iut.authentication.JWTKeyFactory;

@Path("/authenticate")
public class AuthRessource {
    
    private String base64SecretBytes;

    @Context
    private UriInfo uriInfo;
    
    public AuthRessource() {
	base64SecretBytes = JWTKeyFactory.getSecretBytes();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(@FormParam("login") String login, @FormParam("passwd") String passwd) {
	// Vérifier l'utilisateur et le mot de passe
	if ( login.equals("Yvan") ) {
	    // Si c'est bon, on créé un token qu'on renvoie au client
	    String id = UUID.randomUUID().toString().replace("-", "");
	    Date now = new Date();
	    
	    String token = Jwts.builder()
		.setId(id)
                .setSubject(login)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
		.signWith(SignatureAlgorithm.HS256, base64SecretBytes)
		.compact();

	    return Response.ok().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).build();
	}

	throw new WebApplicationException("login ou mot de passe incorrect", Response.Status.UNAUTHORIZED);
    }
}
