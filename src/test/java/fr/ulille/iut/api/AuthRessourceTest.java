package fr.ulille.iut;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Entity;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Form;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AuthRessourceTest {
    private HttpServer server;
    private WebTarget target;

    private final String authURL = "/api/v1/authenticate";
    
    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        target = ClientBuilder.newClient().target(Main.BASE_URI);
    }
    
    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void testAuthenticateGoodUser() {
        Form form = new Form();
        form.param("login", "Yvan");
        form.param("passwd", "GoodPass");

	Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
	Response response = target.path(authURL).request().post(formEntity);

	assertEquals(response.getStatus(), 200);
    }

	
}
